export default [
  {
    "category":"Entertainment: Video Games",
    "type":"multiple",
    "difficulty":"easy",
    "question":"What was the name of the cancelled sequel of Team Fortress?",
    "correct_answer":"Team Fortress 2: Brotherhood of Arms",
    "incorrect_answers":["Team Fortress 2: Desert Mercenaries","Team Fortress 2: Operation Gear Grinder","Team Fortress 2: Return to Classic"]
  },
  {
    "category":"General Knowledge",
    "type":"multiple",
    "difficulty":"easy",
    "question":"What was the nickname given to the Hughes H-4 Hercules, a heavy transport flying boat which achieved flight in 1947?",
    "correct_answer":"Spruce Goose",
    "incorrect_answers":["Noah&#039;s Ark","Fat Man","Trojan Horse"]
  },
  {
    "category":"Entertainment: Cartoon & Animations",
    "type":"multiple",
    "difficulty":"medium",
    "question":"In &quot;The Amazing World of Gumball&quot;, who is the principal of Elmore Junior High?",
    "correct_answer":"Principal Brown",
    "incorrect_answers":["Principal Small","Principal Brawn","Principal Simeon"]
  },
  {
    "category":"Entertainment: Japanese Anime & Manga",
    "type":"multiple",
    "difficulty":"hard",
    "question":"The &quot;To Love-Ru&quot; Manga was started in what year?",
    "correct_answer":"2006",
    "incorrect_answers":["2007","2004","2005"]
  },
  {
    "category":"Entertainment: Television",
    "type":"multiple",
    "difficulty":"hard",
    "question":"Which of these Nickelodeon game shows aired first?",
    "correct_answer":"Double Dare",
    "incorrect_answers":["Nick Arcade","Finders Keepers","Nickelodeon Guts"]
  }
]
