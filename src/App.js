import React from 'react'
import Start from './components/Start'
import Quiz from './components/Quiz'

export default function App () {
  const [isStart, setIsStart] = React.useState(false)
  const [quiz, setQuiz] = React.useState(() => retrieveQuestions())

  React.useEffect(() => {
    retrieveQuestions()
  },[isStart])

  function startQuiz () {
    if (!isStart) {
      setIsStart(prev => !isStart)
    }
  }

  function htmlDecode(input) {
    var doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
  }

  function retrieveQuestions () {
    const url = 'https://opentdb.com/api.php?amount=5&type=multiple&difficulty=easy'
    fetch(url)
    .then(res => res.json())
    .then(data => {
      const quizzes = data.results.map((quiz) => {
        quiz.question = htmlDecode(quiz.question)
        quiz.correct_answer = htmlDecode(quiz.correct_answer)
        quiz.incorrect_answers = quiz.incorrect_answers.map(function(x){return htmlDecode(x);});
        return quiz;
      })
      setQuiz(quizzes)
    })
  }

  return (
    <main>
      {isStart ? <Quiz quiz={quiz} retrieveQuestions={retrieveQuestions} setIsStart={setIsStart} /> : <Start startQuiz={startQuiz} />}
    </main>
  )
}