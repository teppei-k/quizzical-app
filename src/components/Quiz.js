import React from 'react'
// import Question from './Question'
import {nanoid} from 'nanoid'
import arrayShuffle from 'array-shuffle'

export default function Quiz (props) {
  const [isChecking, setIsChecking] = React.useState(false)
  const [answerList, setAnswerList] = React.useState(() => newQuiz(props))
  const [point, setPoint] = React.useState(0)
  const quizWrapper = document.getElementById('quiz-wrapper')

  console.log(props.quiz)

  function newQuiz (props) {
    const quiz = props.quiz
    return quiz.map(item => {
      const id = nanoid();
      const correct_answer = (
        {
          question: item.question,
          key: id,
          id: id,
          isCorrect: true,
          answer: item.correct_answer,
          isSelected: false,
          isMatched: false
        }
      )
      const incorrect_answers = item.incorrect_answers.map(answer => {
        const id = nanoid();
        return (
          {
            question: item.question,
            key: id,
            id: id,
            isCorrect: false,
            answer: answer,
            isSelected: false
         }
        )
      })
  
      incorrect_answers.push(correct_answer)
      const shuffledAnswers = arrayShuffle(incorrect_answers)
      return shuffledAnswers
    }) 
  }

  function handleClick (event) {
    setAnswerList(prev => {
      console.log(event)
      console.log(prev)
      return prev.map(answer => {
        return answer.map(item => {
          if(event.target.dataset.question === item.question && event.target.id === item.id) {
            console.log('if')
            return {...item, isSelected: true}
          } else if (event.target.dataset.question === item.question && event.target.id !== item.id) {
            console.log('else if')
            return {...item, isSelected: false}
          } else {
            console.log('else')
            return {...item}
          }
        })
      })
    })
  }

  function checkAnswer () {
    quizWrapper.classList.toggle('answer')
    setAnswerList(prev => {
      return prev.map(answer => {
        return answer.map(item => {
          if(item.isCorrect === true && item.isSelected) {
            setPoint(prev=> prev + 1)
            return {...item, isMatched: true}
          } else {
            return {...item}
          }
        })
      })
    })

    setIsChecking(prev => !prev)
  }

  console.log(answerList)

  return (
    <>
      <div id="quiz-wrapper" className='box-quiz'>
        {
          answerList.map(answer => {
            const items = answer.map(item => {
              return <div
                data-question={item.question}
                data-iscorrect={item.isCorrect ? 'correct' : ''}
                data-ismatched={item.isMatched ? 'matched' : ''}
                id={item.id}
                className={item.isSelected ? 'selected' : ''}
                onClick={handleClick}>{item.answer}</div>
            })
            console.log(items)
            return (
              <>
                <h2>{answer[0].question}</h2>
                <div className='answer-list'>
                  {items}
                </div>
              </>
            )
          })
        }
      </div>
      <div>
        {
          isChecking === true
          ? <>
            <p className='score-board'>You scored {point}/5 correct answers</p>
            <button className='button' onClick={() => props.setIsStart(false)}>Play again</button>
          </>
          : <button className='button' onClick={checkAnswer}>Check answers</button>
        }
      </div>
    </>
  )
}