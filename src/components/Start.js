import React from 'react'

export default function Start (props) {
  return (
    <div className='box-start'>
      <h1>Quizzical</h1>
      <p>Some description if needed</p>
      <button className='button' onClick={props.startQuiz}>Start quiz</button>
    </div>
  )
}